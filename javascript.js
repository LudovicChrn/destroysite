// ajoute un event sur le bouton "myBtn"
document.getElementById("myBtn").addEventListener("click", function() {
        // selectionne la div "content_content" 
        var elementToRotate = document.querySelector(".content_content");

        //ajoute une classe css "rotated180"
        elementToRotate.classList.toggle("rotated180");

        //change le titre de la page
        document.title = "Good luck.";
    
        //fait disparaitre le contenu de la div "content_content"
        document.querySelector(".test2").style.display = "none";
        document.querySelector(".fa-heart").style.display = "none";
        document.querySelector(".content_para").style.display = "none";
    
        //change l'image d'arrière plan 
        document.querySelector(".content_content").style.backgroundImage = "url('./22641.png')";
        //change la couleur d'arrière plan
        document.body.style.backgroundColor = "black";

        //selectionne le bouton et le fait apparaitre à des endroit différents
        var b = document.querySelector("button");
        setInterval(() => {
            var i = Math.floor(Math.random()*1800)+1;
            var j = Math.floor(Math.random()*900)+1;
            b.style.left = i+"px";
            b.style.top = j+"px";
        }, 500);

});